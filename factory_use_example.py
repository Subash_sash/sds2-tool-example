from ToolSample.ToolSample import ToolSample

# this returns an instance of the class with some default stuff filled out
# this method's parameters will vary by class, since different classes
# will need different items set
sample = ToolSample.Factory(18., 12.)
sample.Run()  #this will run the tool
